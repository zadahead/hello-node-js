# Hello React

_A step-by-step NodeJS course - with all the things you need to get started with this very popular server side engine_

## Prerequisites

1. Install VSCode (or other IDE)
2. Install NodeJs (and npm)
3. Make sure you can use your (IDE or cmd) terminal to write `npm --version` and `node --version`

## Points

1. Open Your Camera
2. Ask Questions 
3. Do it yourself


## Course Agenda

1. Javascript on Server? history of NodeJS
1. our first node command
1. process.argv
1. node modules - fs
1. create our own simplt DB
1. CRUD 
1. createServer with http
1. Rest API -> Restaurant?
1. req - `url and method`
1. npm i express
1. create our first endpoints
1. req.params - `/user/:userId `
1. req.body  - `app.use(express.json());`
1. postman
1. nodemon
1. Routing
1. npm scripts - `npm start`
1. Static HTML Files
    - server HTML file
    - replace HTML Content
    - server static files - `app.use(express.static('public'));`
1. Interact our server with a react app
1. Cors
1. Handling Exceptions
1. Middlewares
    - use our own middleware
    - functions chaining
1. Validations
    - Schema Assertion - `npm i ajv`
    - Status Codes
1. Handling Exceptions With status codes
1. Add status codes to our routes
1. Routes, Controllers, Services
1. User Authenticaion and Authorisations








